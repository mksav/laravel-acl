<?php
namespace Mksav\Acl;

class Acl
{
    public function __construct(ControllableInterface $user, array $accessControl = [], array $ignore = [])
    {
        $this->userRole = $user->getRole();
        $this->accessControl = $accessControl;
        $this->ignore = $ignore;
    }

    /**
     * Check if a specific role has access to a route
     * 
     * @param  string  The name of the controller
     * @param  string  The name of the controller method
     * @return bool
     */
    public function checkControllerAccess($controller, $method)
    {
        // Is the controller action ignored for auth purposes?
        if ($this->ignored($controller))
            return true;

        // Check if the user has access to the controller actions at all
        if (array_key_exists($controller, $this->accessControl[$this->userRole]))
            return $this->checkControllerMethod($controller, $method);
        
        return false;
    }

    /**
     * Check if a user has access to a specific part of a view
     * 
     * @param  mixed  String or array of those who SHOULD have access
     * @return bool
     */
    public function partialAccess($allowed)
    {
        return in_array($this->userRole, (array)$allowed);
    }

    /**
     * Check if a controller should be ignored from ACL
     * 
     * @param  string  The name of the controller
     * @return bool
     */
    public function ignored($controller)
    {
        return in_array($controller, $this->ignore);
    }

    /**
     * Check the controller method systematically
     * 
     * @param  string  The name of the controller
     * @param  string  The name of the controller method
     * @return bool
     */
    protected function checkControllerMethod($controller, $method)
    {
        // If the user has blanket access to the controller actions (empty array), let them through
        if ($this->allMethods($controller))
            return true;

        // If the user explicitly DOES NOT have access to a method, return false
        if ($this->noAccess($controller, $method))
            return false;

        // If the user explicity DOES have access, return true
        return $this->access($controller, $method);
    }

    /**
     * Check if the user should have access to all of the actions within the controller (empty array)
     * 
     * @param  string  Name of the controller
     * @return bool
     */
    protected function allMethods($controller)
    {
        return empty($this->accessControl[$this->userRole][$controller]);
    }

    /**
     * Check if the user's role is set for no access to specific controller actions
     * 
     * @param  string  The name of the controller
     * @param  string  The name of the controller method
     * @return bool
     */
    protected function noAccess($controller, $method)
    {
        return array_key_exists('none', $this->accessControl[$this->userRole][$controller]) and in_array($method, $this->accessControl[$this->userRole][$controller]['none']);
    }

    /**
     * Check if the user's role is set to have access only to specific controller actions
     * 
     * @param  string  The name of the controller
     * @param  string  The name of the controller method
     * @return bool
     */
    protected function access($controller, $method)
    {
        return array_key_exists('access', $this->accessControl[$this->userRole][$controller]) and in_array($method, $this->accessControl[$this->userRole][$controller]['access']);
    }
}
