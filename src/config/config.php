<?php

return [
    // Access control list
    'access_control' => [
        // Role definitions
        'admin' => [
            // 'DashboardController' => [], // empty array signifies full access
            // 'PurchaseOrdersController' => [
            //     'access' => ['index'] // 'access' key specifies a whitelist of controller methods the role has access to
            // ],
            // 'OrdersController' => [
            //     'none' => ['create'] // 'none' key specifies a blacklist of controller method the roles has NO access to
            // ],
        ],
        'user' => [
            // 'DashboardController' => [],
        ]
    ],
    // List of controller to ignore when running any access control checks
    'ignore' => [
        // i.e. 'LoginController',
    ]
];
