<?php

use Mksav\Acl\Acl;

class AclTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    public function setUp()
    {
        parent::setUp();
        
        $user = Mockery::mock('Mksav\Acl\ControllableInterface');
        $user->shouldReceive('getRole')->once()->andReturn('admin');

        $accessControl = $this->getAccessControlConfig();
        $ignore = $this->getIgnoredControllers();

        $this->acl = new Acl($user, $accessControl, $ignore);       
    }

    public function getAccessControlConfig()
    {
        return [
            'admin' => [
                'DashboardController' => [],
                'PurchaseOrdersController' => [
                    'access' => ['index']
                ],
                'OrdersController' => [
                    'none' => ['create']
                ],
            ],
            'user' => [
                'DashboardController' => [],
            ]
        ];
    }

    public function getIgnoredControllers()
    {
        return [
            'LoginController'
        ];
    }

    public function testIgnoredControllerIsIgnored()
    {
        $this->assertTrue($this->acl->ignored('LoginController'));
    }

    public function testControllerIsNotIgnored()
    {
        $this->assertFalse($this->acl->ignored('DashboardController'));
    }

    public function testUserHasAccess()
    {
        $this->assertTrue($this->acl->checkControllerAccess('DashboardController', 'index'));
    }

    public function testUserHasNoAccess()
    {
        $this->assertFalse($this->acl->checkControllerAccess('OrdersController', 'index'));
    }

    public function testRoleHasPartialAccess()
    {
        $this->assertTrue($this->acl->partialAccess('admin'));
    }

    public function testRoleHasNoPartialAccess()
    {
        $this->assertFalse($this->acl->partialAccess('user'));
    }
}
