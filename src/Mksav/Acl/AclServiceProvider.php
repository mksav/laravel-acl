<?php namespace Mksav\Acl;

use Illuminate\Support\ServiceProvider;
use Auth;
use Config;

class AclServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot()
	{
		$this->package('mksav/laravel-acl');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// Bind the Acl implementation for the Facade to work
        $this->app->bind('acl', function() {
            return new Acl(Auth::user(), Config::get('laravel-acl::access_control'), Config::get('laravel-acl::ignore'));
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('acl');
	}

}