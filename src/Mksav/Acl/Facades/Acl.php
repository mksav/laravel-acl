<?php
namespace Mksav\Acl\Facades;

use Illuminate\Support\Facades\Facade;

class Acl extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'acl';
    }
}
