<?php
namespace Mksav\Acl;

interface ControllableInterface
{
    /**
     * Should return the role or group that a user is a member of for access control
     */
    public function getRole();
}
